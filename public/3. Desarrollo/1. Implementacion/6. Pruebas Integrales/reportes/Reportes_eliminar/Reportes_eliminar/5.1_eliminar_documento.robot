*** Settings ***
Documentation    Proyecto de pruebas para Gestiona
Library    SeleniumLibrary

*** Variables ***
${url}=     http://10.168.100.16/#/login
${navegador}=   chrome
${SLEEP}=   1


*** Test Cases ***
caso 1
    [Documentation]    Prueba de Login
    [Tags]    prueba_1
    open browser   ${url}   ${navegador}
    maximize browser window
    sleep    ${SLEEP}
    Input text    //input[contains(@formcontrolname,'usuario')]  dmadmin
    sleep    ${SLEEP}
    Input Text    //input[contains(@formcontrolname,'contrasena')]     gestiona
    sleep    ${SLEEP}
    capture page screenshot     login-elimina-documento.png
    Click Element   //span[contains(.,'Iniciar sesión')]

caso 2
    [Documentation]    Panel Control
    [Tags]    prueba_2
    sleep    ${SLEEP}
    Click Element   //a[@href='#/dashboard/explorador_archivos']
    capture page screenshot     menu-elimina-documento.png

caso 3
    [Documentation]    Arbol de documentos
    [Tags]    prueba_3

    sleep    ${SLEEP}
    Click Element   //span[contains(@class,'p-menuitem-icon mi mi-inventory_2 ng-star-inserted')]
    sleep    ${SLEEP}
    Click Element   (//span[contains(@class,'p-tree-toggler-icon pi pi-fw pi-chevron-right')])[1]
    sleep    ${SLEEP}
    Click Element   (//span[contains(@class,'p-tree-toggler-icon pi pi-fw pi-chevron-right')])[1]
    sleep    ${SLEEP}
    Click Element   //div[@role='treeitem'][contains(.,'Area Cuatro')]
    capture page screenshot     lista_de_documentos-elimina-documento.png

caso 4
    [Documentation]    Check-out
    [Tags]    prueba_4

    sleep    ${SLEEP}
    Click Element   (//div[contains(@role,'checkbox')])[1]
    capture page screenshot     select-documento-elimina-documento.png
   # sleep    ${SLEEP}
   # Click Element   (//div[contains(@role,'checkbox')])[2]
    sleep    ${SLEEP}
    Click Element   //button[@ptooltip='Eliminar archivo']
    sleep    ${SLEEP}
    Click Element   //button[@label='Aceptar']
    sleep    ${SLEEP}
    capture page screenshot     arhivodescargado-elimina-documento.png

caso 5
    [Documentation]    cerrar sección Usuario
    [Tags]    prueba_5

    Sleep    5
    Click Element    //span[@class="p-button-icon pi pi-power-off"]
    capture page screenshot     cerrar_seccion-elimina-documento.png
    Sleep    1
    Click Element    xpath=(//span[@class="p-button-label"])[4]
    Sleep    1
    close browser




*** Keywords ***

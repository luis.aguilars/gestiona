docker rm -f $(docker ps -a -q)
docker container prune -f
docker image prune -a -f
docker network prune -f
time DOCKER_BUILDKIT=1 docker image build --no-cache -f auth.dockerfile -t auth:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f bitobjetoversion.dockerfile -t bitobjetoversion:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f document.dockerfile -t document:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f folders.dockerfile -t folders:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f gabinetes.dockerfile -t gabinetes:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f privilegios.dockerfile -t privilegios:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f roles.dockerfile -t roles:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f usuarios.dockerfile -t usuarios:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f ws.dockerfile -t ws:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f archivo.dockerfile -t archivo:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f origencarga.dockerfile -t origencarga:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f catapp.dockerfile -t catapp:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f log.dockerfile -t log:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f contenidos.dockerfile -t contenidos:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f compartir.dockerfile -t compartir:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f efirma.dockerfile -t efirma:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f bitws.dockerfile -t bitws:v1 --no-cache .
time DOCKER_BUILDKIT=1 docker image build --no-cache -f front.dockerfile -t front:v1 --no-cache .
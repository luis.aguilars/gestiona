# Stage 1: Compile and Build angular codebase

# Use official node image as the base image
#FROM node:latest as build

# Set the working directory
#WORKDIR /usr/local/app

# Add the source code to app
#COPY ./PortalWebGestiona/portal-gestiona/ /usr/local/app/

# Install all the dependencies
#RUN npm install

# Generate the build of the application
#RUN npm run build


# Stage 2: Serve app with nginx server

# Use official nginx image as the base image
FROM nginx:latest

# Copy the build output to replace the default nginx contents.
#COPY --from=build /usr/local/app/dist/portal-gestiona /usr/share/nginx/html
COPY ./portal/portal-gestiona /usr/share/nginx/html

#FROM httpd:2.4
#COPY ./PortalWebGestiona/portal-gestiona/dist/portal-gestiona /usr/local/apache2/htdocs/

# Expose port 80
EXPOSE 80
EXPOSE 443

#INIT TOOLS
apt-get update -y
apt-get upgrade -y
apt-get install supervisor -y
apt-get install ca-certificates curl gnupg lsb-release npm -y
apt-get install openjdk-11-jre openjdk-11-jdk -y
npm install -g @angular/cli
#install docker
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update -y
apt-get install docker-ce docker-ce-cli containerd.io -y

#install docker-compose
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
systemctl stop apache2
apt-get purge apache2 apache2-utils apache2-bin apache2.2-common -y
apt-get autoremove -y
rm -rf /usr/sbin/apache2 /usr/lib/apache2 /etc/apache2 /usr/share/apache2 /usr/share/man/man8/apache2.8.gz
mkdir /etc/lib/
mkdir /etc/lib/ges/
mkdir /data
mkdir /data/system_repository
cp ./tools/ocr/ocr.conf /etc/supervisor/conf.d/
cp ./JARS/ocr.jar /etc/lib/ges/ocr